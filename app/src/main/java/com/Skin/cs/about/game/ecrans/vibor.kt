package com.Skin.cs.about.game.ecrans

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.Skin.cs.about.game.R
import kotlinx.coroutines.delay

@Composable
fun Vibor(g1:()->Unit,g2:()->Unit,g3:()->Unit) {


    val param = LocalContext.current as Activity

    var scale1 by remember { mutableStateOf(1f) }
    var scale2 by remember { mutableStateOf(1f) }
    var scale3 by remember { mutableStateOf(1f) }
    var scale4 by remember { mutableStateOf(1f) }

    val jzgfjkhg = LocalConfiguration.current.screenHeightDp


    var clicked1 by remember{
        mutableStateOf(false)
    }
    var clicked2 by remember{
        mutableStateOf(false)
    }
    var clicked3 by remember{
        mutableStateOf(false)
    }


    LaunchedEffect(clicked1,clicked2,clicked3 )
    {
        delay(100)
        if(clicked1)
        {
            g1.invoke()
        }
        if(clicked2)
        {
            g2.invoke()
        }
        if(clicked3)
        {
            g3.invoke()
        }
    }
    Box(modifier = Modifier.fillMaxSize()) {
        Image(
            painter = painterResource(id = R.drawable.mainbg),
            contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
        )

        Column(
            modifier = Modifier.align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy((jzgfjkhg * 0.05).dp)
        ) {
            val animatedScale1 by animateFloatAsState(targetValue = scale1, animationSpec = tween(100),
                label = ""
            )
            val animatedScale2 by animateFloatAsState(targetValue = scale2, animationSpec = tween(100))
            val animatedScale3 by animateFloatAsState(targetValue = scale3, animationSpec = tween(100))
            val animatedScale4 by animateFloatAsState(targetValue = scale4, animationSpec = tween(100))

            Image(
                painter = painterResource(id = R.drawable.gknife),
                contentDescription = "",
                modifier = Modifier
                    .scale(animatedScale1)
                    .clickable(MutableInteractionSource(), null) {
                        scale1 = 0.5f
                        clicked1 = true
                    }
            )
            Image(
                painter = painterResource(id = R.drawable.gweapon),
                contentDescription = "",
                modifier = Modifier
                    .scale(animatedScale2)
                    .clickable(MutableInteractionSource(), null) {
                        scale2 = 0.5f
                        clicked2 = true
                    }
            )
            Image(
                painter = painterResource(id = R.drawable.gthings),
                contentDescription = "",
                modifier = Modifier
                    .scale(animatedScale3)
                    .clickable(MutableInteractionSource(), null) {
                        scale3 = 0.5f
                        clicked3 = true
                    }
            )
            Image(
                painter = painterResource(id = R.drawable.exitbut),
                contentDescription = "",
                modifier = Modifier
                    .scale(animatedScale4)
                    .clickable {
                        scale4 = 0.5f
                        param.finish()
                    }
            )
        }
    }

    BackHandler {}

}