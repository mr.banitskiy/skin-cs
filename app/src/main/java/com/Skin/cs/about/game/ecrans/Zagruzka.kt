package com.Skin.cs.about.game.ecrans

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.Skin.cs.about.game.R
import com.Skin.cs.about.game.ui.theme.Green
import com.Skin.cs.about.game.ui.theme.White

@Composable
fun Zagruzka(g2:()->Unit) {


    val fh = LocalConfiguration.current.screenHeightDp
    val fw = LocalConfiguration.current.screenWidthDp

    val progress = remember {
        Animatable(0f)
    }

    LaunchedEffect(key1 =Unit)
    {
        progress.animateTo(1f, animationSpec = tween(3000))
        g2.invoke()
    }

    Box(modifier = Modifier.fillMaxSize())
    {
        Image(painter = painterResource(id = R.drawable.loadbg), contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds)
        LinearProgressIndicator(progress = progress.value,modifier = Modifier
            .align(Alignment.Center)
            .padding(top = (fh*0.35).dp)
            .width((fw*0.8).dp)
            .height((fh*0.03).dp)
            .clip(
                RoundedCornerShape(16.dp)
            ), trackColor = White,color = Green
        )
    }
}