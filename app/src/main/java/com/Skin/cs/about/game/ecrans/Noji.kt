package com.Skin.cs.about.game.ecrans

import android.view.MotionEvent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.Skin.cs.about.game.R
import com.Skin.cs.about.game.Text
import com.Skin.cs.about.game.ui.theme.White
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Noji(gb:()->Unit) {

    val scale = remember{
        mutableStateOf(1f)
    }

    val c = LocalContext.current
    val h = LocalConfiguration.current.screenHeightDp
    val knifes = remember{
        mutableListOf(
            R.drawable.karadoppler,
            R.drawable.bowie,
            R.drawable.bayonetgamma,
            R.drawable.bayonetdopple,
            R.drawable.tigertooth,
            R.drawable.m9bayonet,
            R.drawable.talon,
            R.drawable.butterfly,
            R.drawable.casehardened,
            R.drawable.huntsman,
            R.drawable.talon2,
            R.drawable.flipdoppler,
            R.drawable.talonultraviolet,
            R.drawable.ursus,
            R.drawable.bowie2,
        )
    }

    val knife = remember{
        mutableListOf(
            c.getString(R.string.first),
            "Bowie Knife",
            "Bayonet | Gamma",
            c.getString(R.string.dopler),
            "Tiger Tooth",
            "M9 Bayonet",
            "Talon Knife",
            c.getString(R.string.bknife),
            "Case Hardened",
            "Huntsman Knife",
            "Talon Knife",
            "Flip Knife | Doppler",
            c.getString(R.string.tkinfe),
            "Ursus Knife",
            "Bowie Knife",
        )
    }

    val number = remember{
        mutableStateOf(0)
    }

    val scaleback = remember{
        mutableStateOf(1f)
    }
    val cs = CoroutineScope(Dispatchers.IO)
    Box(modifier = Modifier.fillMaxSize())
    {


        Image(painter = painterResource(id = R.drawable.mainbg), contentDescription ="",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds)

        Image(painter = painterResource(id = R.drawable.back), contentDescription ="",
            modifier = Modifier
                .align(Alignment.TopStart)
                .padding((h*0.05).dp)
                .scale(scaleback.value)
                .pointerInteropFilter {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> {
                            scaleback.value = 1.5f
                        }

                        MotionEvent.ACTION_UP -> {
                            scaleback.value = 1f
                            if(number.value == 0)
                            {
                                gb.invoke()
                            }
                            else
                            {
                                number.value-=1
                            }
                        }
                        else -> {}
                    }
                    true
                })

        Column(modifier = Modifier
            .align(Alignment.Center)
            .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(30.dp)) {
            Text(text = knife[number.value], size = 30 , modifier =Modifier , color = White )
            Image(painter = painterResource(id = knifes[number.value]), contentDescription ="",
                modifier = Modifier
                    .fillMaxWidth())
        }
        Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
            modifier = Modifier.
            align(Alignment.BottomCenter)
                .padding(bottom = ((h*0.1).dp))
                .scale(scale.value)
                .pointerInteropFilter {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> {
                            scale.value = 0.5f
                        }
                        MotionEvent.ACTION_UP -> {

                            if(number.value<14)
                            {
                                cs.launch {
                                    delay(50)
                                    scale.value = 1f
                                    number.value+=1
                                }

                            }
                            else
                            {
                                number.value=0
                            }

                        }
                        else -> {}
                    }
                    true
                }
        )
    }
}