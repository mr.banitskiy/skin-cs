package com.Skin.cs.about.game

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.sp

@Composable
fun Text(text: String, size: Int, modifier: Modifier, color: Color) {
    Text(text = text, fontSize = size.sp, color = color,
        fontFamily =  FontFamily(Font(R.font.passero))
    )
}