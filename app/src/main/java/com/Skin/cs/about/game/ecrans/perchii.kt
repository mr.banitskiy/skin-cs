package com.Skin.cs.about.game.ecrans

import android.view.MotionEvent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.Skin.cs.about.game.R
import com.Skin.cs.about.game.Text
import com.Skin.cs.about.game.ui.theme.White
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Perchi(nazad:()->Unit) {

    val razmer = remember{
        mutableStateOf(1f)
    }

    val govno = LocalContext.current
    val zachem = LocalConfiguration.current.screenHeightDp
    val perchatki = remember{
        mutableListOf(
            R.drawable.hudraman,
            R.drawable.spec,
            R.drawable.hand,
            R.drawable.sportik,
            R.drawable.specmarble,
            R.drawable.sportamph,
            R.drawable.broken,
            R.drawable.driver,
            R.drawable.driver2,
            R.drawable.sportik2,
            R.drawable.fang,
            R.drawable.specmogul,
            R.drawable.sportbg,
            R.drawable.sportik3,

        )
    }

    val nazviperchey = remember{
        mutableListOf(
            govno.getString(R.string.perchi1),
            govno.getString(R.string.perchi2),
            govno.getString(R.string.perchi3),
            govno.getString(R.string.perchi4),
            govno.getString(R.string.perchi5),
            govno.getString(R.string.perchi6),
            govno.getString(R.string.perchi7),
            govno.getString(R.string.perchi8),
            govno.getString(R.string.perchi9),
            govno.getString(R.string.perchi10),
            govno.getString(R.string.perchi11),
            govno.getString(R.string.perchi12),
            govno.getString(R.string.perchi13),
            govno.getString(R.string.perchi14),
        )
    }

    val chtozaperchi = remember{
        mutableStateOf(0)
    }

    val babamka = remember{
        mutableStateOf(1f)
    }

    val cs = CoroutineScope(Dispatchers.IO)
    Box(modifier = Modifier.fillMaxSize())
    {
        Image(painter = painterResource(id = R.drawable.mainbg), contentDescription ="",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds)

        Image(painter = painterResource(id = R.drawable.back), contentDescription ="",
            modifier = Modifier
                .align(Alignment.TopStart)
                .padding((zachem * 0.05).dp)
                .scale(babamka.value)
                .pointerInteropFilter {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> {
                            babamka.value = 1.5f
                        }

                        MotionEvent.ACTION_UP -> {
                            babamka.value = 1f
                            if (chtozaperchi.value == 0) {
                                nazad.invoke()
                            } else {
                                chtozaperchi.value -= 1
                            }
                        }

                        else -> {}
                    }
                    true
                })

        Column(modifier = Modifier
            .align(Alignment.Center)
            .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(30.dp)) {
            Text(text = nazviperchey[chtozaperchi.value], size = 30 , modifier =Modifier , color = White )
            Image(painter = painterResource(id = perchatki[chtozaperchi.value]), contentDescription ="",
                modifier = Modifier
                    .fillMaxWidth())
        }
        Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
            modifier = Modifier.
            align(Alignment.BottomCenter)
                .padding(bottom = ((zachem*0.1).dp))
                .scale(razmer.value)
                .pointerInteropFilter {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> {
                            razmer.value = 0.5f
                        }

                        MotionEvent.ACTION_UP -> {

                            if(chtozaperchi.value<13)
                            {
                                cs.launch {
                                    delay(50)
                                    chtozaperchi.value+=1
                                    razmer.value = 1f
                                }

                            }
                            else
                            {
                                chtozaperchi.value=0
                                razmer.value = 1f
                            }

                        }
                        else -> {}
                    }
                    true
                }
        )
    }
}