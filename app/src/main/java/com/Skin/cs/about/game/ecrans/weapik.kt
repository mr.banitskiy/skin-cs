package com.Skin.cs.about.game.ecrans

import android.view.MotionEvent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.Skin.cs.about.game.R
import com.Skin.cs.about.game.Text
import com.Skin.cs.about.game.ui.theme.White

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Weapik(nazad:()->Unit) {

    val esheparasha = remember{
        mutableStateOf(1f)
    }

    val contik = LocalContext.current
    val visota = LocalConfiguration.current.screenHeightDp
    val gunchiki = remember{
        mutableListOf(
            R.drawable.de,
            R.drawable.akblood,
            R.drawable.akasi,
            R.drawable.usp,
            R.drawable.aksl,
            R.drawable.awnn,
            R.drawable.akemp,
            R.drawable.aknight,
            R.drawable.akanub,
            R.drawable.awpasi,
            R.drawable.mtakau,
            R.drawable.akneon,
            R.drawable.afrant,
            R.drawable.akhuper,
            R.drawable.akpha,

        )
    }

    val orujiya = remember{
        mutableListOf(
            contik.getString(R.string.de),
            contik.getString(R.string.ab),
            contik.getString(R.string.az),
            contik.getString(R.string.up),
            contik.getString(R.string.ass),
            contik.getString(R.string.ann),
            "AK-47 | The Empress",
            contik.getString(R.string.aknnn),
            contik.getString(R.string.aknunn),
            "AWP | Asiimov",
            "M4A4 | Temukau",
            contik.getString(R.string.akneon),
            "AK-47 | Frontside",
            "M4A1-S | Hyper",
            contik.getString(R.string.akP)
        )
    }

    val kakoy = remember{
        mutableStateOf(0)
    }

    val parashka = remember{
        mutableStateOf(1f)
    }

    Box(modifier = Modifier.fillMaxSize())
    {
        Image(painter = painterResource(id = R.drawable.mainbg), contentDescription ="",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds)

        Image(painter = painterResource(id = R.drawable.back), contentDescription ="",
            modifier = Modifier
                .align(Alignment.TopStart)
                .padding((visota*0.05).dp)
                .scale(parashka.value)
                .pointerInteropFilter {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> {
                            parashka.value = 1.5f
                        }
                        MotionEvent.ACTION_UP -> {
                            parashka.value = 1f
                            if(kakoy.value == 0)
                            {
                                nazad.invoke()
                            }
                            else
                            {
                                kakoy.value-=1
                            }
                        }
                        else -> {}
                    }
                    true
                })

        Column(modifier = Modifier
            .align(Alignment.Center)
            .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(30.dp)) {
            Text(text = orujiya[kakoy.value], size = 30 , modifier =Modifier , color = White )
            Image(painter = painterResource(id = gunchiki[kakoy.value]), contentDescription ="",
                modifier = Modifier
                    .fillMaxWidth())
        }
        Image(painter = painterResource(id = R.drawable.next), contentDescription = "",
            modifier = Modifier.
            align(Alignment.BottomCenter)
                .padding(bottom = ((visota*0.1).dp))
                .scale(esheparasha.value)
                .pointerInteropFilter {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> {
                            esheparasha.value = 0.5f
                        }

                        MotionEvent.ACTION_UP -> {
                            esheparasha.value = 1f
                            if(kakoy.value<14)
                            {
                                kakoy.value+=1
                            }
                            else
                            {
                                kakoy.value=0
                            }

                        }
                        else -> {}
                    }
                    true
                }
        )
    }
}