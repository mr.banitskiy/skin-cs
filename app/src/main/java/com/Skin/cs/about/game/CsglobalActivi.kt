package com.Skin.cs.about.game

import android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.Skin.cs.about.game.ecrans.Noji
import com.Skin.cs.about.game.ecrans.Perchi
import com.Skin.cs.about.game.ecrans.Vibor
import com.Skin.cs.about.game.ecrans.Weapik
import com.Skin.cs.about.game.ecrans.Zagruzka
import com.Skin.cs.about.game.ui.theme.SkinCsTheme

class CsglobalActivi : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val wc = WindowCompat.getInsetsController(window, window.decorView)
        wc.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        wc.hide(WindowInsetsCompat.Type.statusBars())
        requestedOrientation = SCREEN_ORIENTATION_PORTRAIT
        setContent {
            SkinCsTheme {
                val c = rememberNavController()

                NavHost(navController = c, startDestination = getString(R.string.perviy) )
                {
                    composable(getString(R.string.perviy))
                    {
                        Zagruzka(g2 = {c.navigate(getString(R.string.vtoroy))})
                    }

                    composable(getString(R.string.vtoroy))
                    {
                        Vibor(g1 = {c.navigate(getString(R.string.tretiy)) },
                            g2 = { c.navigate(getString(R.string.chetvertiy)) },
                            g3 = {c.navigate(getString(R.string.patiy))})
                    }
                    composable(getString(R.string.tretiy))
                    {
                        Noji(gb = {c.navigate(getString(R.string.vtoroy))})
                    }
                    composable(getString(R.string.chetvertiy))
                    {
                        Weapik(nazad = {c.navigate(getString(R.string.vtoroy))})
                    }
                    composable(getString(R.string.patiy))
                    {
                        Perchi(nazad = {c.navigate(getString(R.string.vtoroy))})
                    }

                }
            }
        }
    }
}